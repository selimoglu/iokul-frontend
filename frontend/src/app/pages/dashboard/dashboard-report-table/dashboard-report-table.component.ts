/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableData } from '../../../@core/interfaces/common/smart-table';
import { NbComponentShape, NbComponentSize, NbComponentStatus } from '@nebular/theme';
@Component({
  selector: 'dashboard-report-table',
  templateUrl: './dashboard-report-table.component.html',
  styleUrls: ['./dashboard-report-table.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class DashboardReportTableComponent {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'Ders Adı',
        type: 'number',
      },
      firstName: {
        title: 'Kısa Adı',
        type: 'string',
      },
      lastName: {
        title: 'HDS',
        type: 'string',
      },
      username: {
        title: 'Yerleşme Biçimi',
        type: 'string',
      },
      email: {
        title: 'Seçmeli',
        type: 'string',
      },
      age: {
        title: 'Bölünebilme',
        type: 'number',
      },
      grup: {
        title: 'Grup',
        type: 'number',
      },
    },
  };
  type = 'today';
  typeTexts = [{ text: 'Today', value: 'today' },
  { text: 'Yesterday', value: 'yesterday' }];
  source: LocalDataSource = new LocalDataSource();
  shapes: NbComponentShape[] = [ 'round' ];
  constructor(private service: SmartTableData) {
    const data = this.service.getData();
    this.source.load(data);
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
