import {Component, OnDestroy} from '@angular/core';
import { takeWhile } from 'rxjs/operators' ;

@Component({
  selector: 'ngx-siniflar',
  styleUrls: ['./siniflar.component.scss'],
  templateUrl: './siniflar.component.html',
})
export class ClassesComponent implements OnDestroy {

  private alive = true;



  constructor() {

  }


  ngOnDestroy() {
    this.alive = false;
  }
}
