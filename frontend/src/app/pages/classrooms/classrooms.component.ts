/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import {Component, OnDestroy} from '@angular/core';
import { takeWhile } from 'rxjs/operators' ;

@Component({
  selector: 'ngx-classrooms',
  styleUrls: ['./classrooms.component.scss'],
  templateUrl: './classrooms.component.html',
})
export class ClassroomsComponent implements OnDestroy {

  private alive = true;



  constructor() {

  }


  ngOnDestroy() {
    this.alive = false;
  }
}
