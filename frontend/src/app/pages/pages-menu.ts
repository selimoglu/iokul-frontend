import { NbMenuItem } from '@nebular/theme';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PagesMenu {

  getMenu(): Observable<NbMenuItem[]> {
    const menu1: NbMenuItem[] = [
      {
        title: 'Dersler',
        icon: 'book-open-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Sınıflar',
        icon: 'flag-outline',
        link: '/pages/siniflar',
        children: undefined,
      },
      {
        title: 'Derslikler',
        icon: 'keypad-outline',
        link: '/pages/classrooms',
        home: true,
        children: undefined,
      },
      {
        title: 'Öğretmenler',
        icon: 'person-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },

      {
        title: 'Öğrenciler',
        icon: 'github-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Gruplar',
        icon: 'people-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Atamalar',
        icon: 'person-add-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Ders Programları',
        icon: 'map-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Seçmeli/Kurs',
        icon: 'edit-2-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      }, {
        title: 'Sınav',
        icon: 'question-mark-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Ek Ders Hesaplama',
        icon: 'edit-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },

       {
        title: 'Yoklama',
        icon: 'checkmark-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
       {
        title: 'Akıllı Tahta',
        icon: 'monitor-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
       {
        title: 'Aferin',
        icon: 'star-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Anket',
        icon: 'pie-chart-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Sms',
        icon: 'message-circle-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },
      {
        title: 'Veri İşlemler',
        icon: 'wifi-outline',
        link: '/pages/dashboard',
        home: true,
        children: undefined,
      },

    ];
 


    return of([...menu1,]);
  }
}
